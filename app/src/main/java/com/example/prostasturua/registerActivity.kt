package com.example.prostasturua

import android.content.Intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.example.prostasturua.profileActivity as profileActivity1

class registerActivity : AppCompatActivity() {

    private lateinit var editName : EditText
    private lateinit var editSurname: EditText
    private lateinit var editAge: EditText
    private lateinit var registerEmail : EditText
    private lateinit var registerPassword: EditText
    private lateinit var registerButton : Button
    private lateinit var backButton : Button








    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
        registerListeners()
        backListener()



    }

    private fun init(){

        editName = findViewById(R.id.editName)
        editSurname = findViewById(R.id.editSurname)
        editAge = findViewById(R.id.editAge)
        registerEmail = findViewById(R.id.editEmailAd)
        registerPassword = findViewById(R.id.editPassword)
        registerButton = findViewById(R.id.buttonReg)
        backButton = findViewById(R.id.buttonBack)


    }
    private fun registerListeners(){
        registerButton.setOnClickListener() {

            val name = editName.text.toString()
            val surname = editSurname.text.toString()
            val age = editAge.text.toString()
            val email = registerEmail.text.toString()
            val password = registerPassword.text.toString()






            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { result ->
                    if (result.isSuccessful) {

                        Toast.makeText(this, "რეგისტრაცია წარმატებით გაიარეთ!", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        Toast.makeText(
                            this,
                            "რეგისტრაცია ვერ მოხერხდა , ცადეთ ხელახლა",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (password.length < 9 || !password.contains("[0-9]".toRegex()) || !password.contains("[a-z]".toRegex())) {
                        Toast.makeText(this,"პაროლი უნდა შეიცავდეს 9ზე მეტ სიმბოლოს!",Toast.LENGTH_SHORT)
                        return@addOnCompleteListener
                    }
                    val intent = Intent(this, profileActivity1::class.java)
                    intent.putExtra("Name",name)
                    intent.putExtra("Surname", surname)
                    intent.putExtra("Age",age)



                }

        }


    }
    private fun backListener(){

        backButton.setOnClickListener(){
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }

    }
}
