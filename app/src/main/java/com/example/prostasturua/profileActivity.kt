package com.example.prostasturua

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import org.w3c.dom.Text

class profileActivity : AppCompatActivity() {


    private lateinit var editTextView: TextView
    private lateinit var editTextView2: TextView
    private lateinit var editTextView3: TextView
    private lateinit var logoutButton: Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        logoutListener()
        init()
        getData()


    }


    private fun init(){
        editTextView = findViewById(R.id.textView1)
        logoutButton = findViewById(R.id.btnLogout)
        editTextView2 = findViewById(R.id.textView2)
        editTextView3 = findViewById(R.id.textView3)
    }
    private fun logoutListener(){

        logoutButton.setOnClickListener(){
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }

    }

    private fun getData(){
        val name = intent.getStringExtra("Name")
        val surname = intent.getStringExtra("Surname")
        val age = intent.getIntExtra("Age",0)
        var intent = intent
        editTextView.text =  "სახელი: $name"
        editTextView2.text =  "გვარი: $surname"
        editTextView3.text = "ასაკი: $age"
    }

}