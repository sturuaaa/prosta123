package com.example.prostasturua

data class UserInfo(

    val name:String = "",
    val surname:String = "",
    val age:String = "",
    val image:String = ""

)
