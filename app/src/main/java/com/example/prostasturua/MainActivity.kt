package com.example.prostasturua

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.widget.Button
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth

class MainActivity() : AppCompatActivity(){

    private lateinit var editEmail: EditText
    private lateinit var editPassword: EditText
    private lateinit var editButton: Button
    private lateinit var editRegButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        loginListeners()
        regListeners()

    }

    private fun init() {
        editEmail = findViewById(R.id.emailText)
        editPassword = findViewById(R.id.passwordText)
        editButton = findViewById(R.id.loginButton)
        editRegButton = findViewById(R.id.regButton)
    }

    private fun loginListeners() {
        editButton.setOnClickListener() {

            val email = editEmail.text.toString()
            val password = editPassword.text.toString()
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            startActivity(Intent(this,profileActivity::class.java))
            finish()
        }
    }
    private fun regListeners(){
        editRegButton.setOnClickListener(){
            startActivity(Intent(this,registerActivity::class.java))
            finish()
        }

    }
}